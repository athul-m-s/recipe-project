import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Recipe } from '../recipes/recipe.model';
import { RecipeService } from '../recipes/recipe.service';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  constructor(private http: HttpClient, private recipeService: RecipeService) { }

  storeRecipes() {
    const recipes = this.recipeService.getRecipes();
    this.http.put('https://fir-959b8.firebaseio.com/recipes.json', recipes).subscribe(res => {
      console.log(res);
    })
  }

  fetchRecipes() {
    return this.http.get<Recipe[]>('https://fir-959b8.firebaseio.com/recipes.json').pipe(map(res => {
      return res.map(res => {
        return {
          ...res,
          ingredient: res.ingredient ? res.ingredient : []
        };
      })
    }), tap(res => {
      this.recipeService.setRecipes(res);
    }))

    //   return this.authService.user.pipe(take(1), exhaustMap(user => {
    //     return this.http.get<Recipe[]>('https://fir-959b8.firebaseio.com/recipes.json',
    //       {
    //         params: new HttpParams().set('auth', user.token)
    //       })
    //   }), map(res => {
    //     return res.map(res => {
    //       return {
    //         ...res,
    //         ingredient: res.ingredient ? res.ingredient : []
    //       };
    //     })
    //   }), tap(res => {
    //     this.recipeService.setRecipes(res);
    //   }))
    // }
  }
}
