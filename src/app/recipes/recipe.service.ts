import { EventEmitter, Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Recipe } from './recipe.model';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  recipeChanged = new Subject<Recipe[]>();

  // private recipes: Recipe[] = [
  //   new Recipe("Test Recipe", "Desc of test recipe", "https://www.finncrisp.com/siteassets/search/smaller/recipes-s.png", [new Ingredient('Meat', 20), new Ingredient('French Fries', 10)]),
  //   new Recipe("Test Recipe", "Desc of test recipe", "https://www.finncrisp.com/siteassets/search/smaller/recipes-s.png", [new Ingredient('Rice', 80), new Ingredient('Vegitables', 12)]),
  //   new Recipe("Test Recipe", "Desc of test recipe", "https://www.finncrisp.com/siteassets/search/smaller/recipes-s.png", [new Ingredient('Meat', 20), new Ingredient('Vegetables', 18)])
  // ]

  private recipes: Recipe[] = [];

  constructor(private shoppingListService: ShoppingListService) { }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipeChanged.next(this.recipes.slice());
  }

  getRecipes() {
    return this.recipes.slice();
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipeChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe) {
    this.recipes[index] = newRecipe;
    this.recipeChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipeChanged.next(this.recipes.slice());
  }
}
