import { Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { AlertComponent } from '../shared/alert/alert.component';
import { PlaceholderDirective } from '../shared/placeholder/placeholder.directive';
import { authResData, AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit, OnDestroy {

  isLoginMode: boolean = true;
  isLoading: boolean = false;
  error: string = null;
  authObs: Observable<authResData>;
  @ViewChild(PlaceholderDirective, { static: false }) alertHost: PlaceholderDirective;
  private closeSub: Subscription;

  constructor(private authService: AuthService, private router: Router, private componenetFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit(): void {
  }

  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }

  onSubmit(form: NgForm) {

    if (!form.valid) {
      return;
    }
    const email = form.value.email;
    const password = form.value.password;
    if (this.isLoginMode) {
      this.isLoading = true;
      this.authObs = this.authService.login(email, password);
    }
    else {
      this.isLoading = true;
      this.authObs = this.authService.signUp(email, password);
    }
    this.authObs.subscribe(res => {
      console.log(res)
      this.isLoading = false;
      this.router.navigate(['/recipes']);
    }, errorMsg => {
      this.error = errorMsg;
      this.showErorAlert(errorMsg);
      this.isLoading = false;
    })
    form.reset();

  }

  onHandleError() {
    this.error = null;
  }

  private showErorAlert(message: string) {
    const alertComponentFactoryRef = this.componenetFactoryResolver.resolveComponentFactory(AlertComponent);
    const hostViewContainerRef = this.alertHost.viewcontainerRef;
    hostViewContainerRef.clear();
    const componenetRef = hostViewContainerRef.createComponent(alertComponentFactoryRef);
    componenetRef.instance.message = message;
    this.closeSub = componenetRef.instance.close.subscribe(x => {
      this.closeSub.unsubscribe();
      hostViewContainerRef.clear();
    })
  }

  ngOnDestroy() {
    this.closeSub ? this.closeSub.unsubscribe() : null;
  }
}
