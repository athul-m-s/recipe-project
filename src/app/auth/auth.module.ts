import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { AuthRouterModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';


@NgModule({
    declarations: [
        AuthComponent
    ],
    imports: [
        AuthRouterModule,
        CommonModule,
        FormsModule,
        SharedModule
    ],
    exports: [

    ]
})
export class AuthModule { }